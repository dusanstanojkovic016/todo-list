import React from "react";
import TodoSingle from "./TodoSingle";

const TodoList = ({ todos, onDelete, onTodoClick, onEdit, fetchData }) => {
  console.log(todos);

  return (
    <>
      <div className="bg-black gap-x-4 gap-y-4 grid grid-cols-4 grid-rows-2 items-center justify-center mt-10">
        {todos.map((singletodo, index) => {
          return (
            <TodoSingle
              key={index}
              {...singletodo}
              onDelete={onDelete}
              // onClick={() => onTodoClick(singletodo.text)}
              onEdit={onEdit}
              fetchData={fetchData}
            />
          );
        })}
      </div>
    </>
  );
};

export default TodoList;
