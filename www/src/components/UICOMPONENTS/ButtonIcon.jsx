import React from "react";

const ButtonIcon = ({ onClick, type, children }) => {
  return (
    <div>
      <button onClick={onClick} type={type}>
        {children}
      </button>
    </div>
  );
};

export default ButtonIcon;
