import React, { useState, useEffect } from "react";
import ApiService from "../../axios";
import Form from "./Form";
import TodoList from "./TodoList";
import { useTodos } from "../../context/todos";

const Todo = () => {
  const [todos, setTodos] = useState([]);
  const [selectedTodo, setSelectedTodo] = useState("");
  const { count, incrementCount } = useTodos();

  const fetchData = async () => {
    try {
      const response = await ApiService().get("/todos");
      setTodos(response.data);
    } catch (err) {
      console.log(err.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleFormSubmit = async (input) => {
    console.log("Form submitted with input:", input);
    console.log("Current list of todos:", todos);
    try {
      const response = await ApiService().post("/add", {
        text: input,
        completed: false,
      });
      const newTodos = [...todos, response.data];
      setTodos(newTodos);
      setSelectedTodo("");
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleDelete = async (id) => {
    try {
      await ApiService().delete(`/todos/${id}`);
      setTodos(todos.filter((todo) => todo._id !== id));
      setSelectedTodo("");
    } catch (err) {
      console.log(err.message);
    }
  };

  // const handleTodoClick = (id) => {
  //   setSelectedTodo(id);
  // };

  const handleEdit = async (id, newText) => {
    try {
      await ApiService().put(`/todos/${id}`, {
        text: newText,
      });
      const updatedTodos = todos.map((todo) =>
        todo._id === id ? { ...todo, text: newText } : todo
      );
      setTodos(updatedTodos);
      setSelectedTodo("");
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <div>
      <div className="flex items-center bg-gray-800 rounded-2xl max-w-[500px] min-h-[5%] mx-auto mt-[65px]">
        <div className="flex items-center mx-auto max-w-[500px] min-h-[50px]">
          <div className="text-gray-50">
            <p>Count: {count}</p>
            <button onClick={incrementCount}>Increment Count</button>
          </div>
          <h1 className="flex items-center text-center text-orange-600 text-2xl font-alumni font-bold tracking-widest">
            {selectedTodo !== "" ? "Edit Todo" : "List of Todos"}
          </h1>
        </div>
      </div>
      {selectedTodo !== "" ? (
        <Form
          onFormSubmit={(input) => handleEdit(selectedTodo, input)}
          initialText={todos.find((todo) => todo._id === selectedTodo).text}
        />
      ) : (
        <Form onFormSubmit={handleFormSubmit} />
      )}
      <TodoList
        todos={todos}
        onDelete={handleDelete}
        // onTodoClick={handleTodoClick}
        onEdit={handleEdit}
        fetchData={fetchData}
      />
    </div>
  );
};

export default Todo;
