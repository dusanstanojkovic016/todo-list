import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { SnackbarProvider } from "notistack";
import { CountProvider } from "./context/todos";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <CountProvider>
        <SnackbarProvider>
          <App />
        </SnackbarProvider>
      </CountProvider>
    </BrowserRouter>
  </React.StrictMode>
);
