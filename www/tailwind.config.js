/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    fontFamily: {
      alumni: ["Alumni sans", "sans-serif"],
      barlow: ["Barlow", "sans-serif"],
    },
  },
  plugins: [],
};
