import React, { createContext, useContext, useState } from "react";

const CountContext = createContext();

export const CountProvider = ({ children }) => {
  const [count, setCount] = useState(0);

  const incrementCount = () => {
    setCount((prevCount) => prevCount + 1);
  };

  const value = { count, incrementCount };

  return (
    <CountContext.Provider value={value}>{children}</CountContext.Provider>
  );
};

export const useTodos = () => useContext(CountContext);
