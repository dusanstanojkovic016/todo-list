import React from "react";

const Button = ({ type, onClick, className, text, disabled }) => {
  return (
    <div>
      <button
        type={type}
        className={`${
          disabled ? "opacity-50" : "opacity-100"
        } absolute inset-y-0 right-0 flex items-center px-4 rounded-full bg-orange-500 p-2 text-white shadow-sm hover:bg-orange-700 ${className}`}
        onClick={onClick}
        disabled={disabled}
      >
        {/* {plusIcon} */}
        {text}
      </button>
    </div>
  );
};

export default Button;
