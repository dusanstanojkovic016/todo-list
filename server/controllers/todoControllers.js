const mongoose = require("mongoose");
const Todos = require("../dbTodos");

//Get Todos
const getTodos = async (req, res) => {
  try {
    const allTodos = await Todos.find({});
    res.status(200).send(allTodos);
  } catch (error) {
    res.status(400).send(error.message);
  }
};

// Create Todos
const createTodo = async (req, res) => {
  const dbTodo = req.body;
  try {
    const newTodo = await Todos.create(dbTodo);
    res.status(201).send(newTodo);
  } catch (error) {
    res.status(500).send(error.message);
  }
};

// Update Todos

// Update Todos
const updateTodo = async (req, res) => {
  const { id } = req.params;
  try {
    // Check if the id is valid
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send(`There is no Todo with id of ${id}`);
    }

    const update = { text: req.body.text, completed: req.body.completed };

    const updatedTodo = await Todos.findOneAndUpdate(
      { _id: req.params.id },
      update
    );

    if (!updatedTodo) {
      return res.status(404).send(`There is no Todo with id of ${id}`);
    }

    res.status(201).send(updatedTodo);
  } catch (error) {
    res.status(500).send(error.message);
  }
};

// Delete Todos

const deleteTodo = async (req, res) => {
  const { id } = req.params;
  try {
    // Check if the id is valid
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).send(`There is no Todo with id of ${id}`);
    }

    const deleteTodo = await Todos.findOneAndDelete({ _id: id });

    res.status(201).send(deleteTodo);
  } catch (error) {
    res.status(500).send(error.message);
  }
};

module.exports = {
  getTodos,
  createTodo,
  updateTodo,
  deleteTodo,
};
