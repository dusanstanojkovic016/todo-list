import React, { useState } from "react";
import Button from "../UICOMPONENTS/Button";
import Input from "../UICOMPONENTS/Input";
import { PlusIcon } from "@heroicons/react/20/solid";
import { useSnackbar } from "notistack";

const Form = ({ onFormSubmit, initialText }) => {
  const [input, setInput] = useState("");
  const { enqueueSnackbar } = useSnackbar("");

  const handleInputChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onFormSubmit(input);
    setInput("");
    if (initialText) {
      enqueueSnackbar("Item successfully updated!", {
        variant: "success",
        autoHideDuration: 2000,
      });
    } else {
      enqueueSnackbar("Item successfully added!", {
        variant: "success",
        autoHideDuration: 2000,
      });
    }
  };

  return (
    <div>
      <div className="relative text-white max-w-[500px] min-h-[5%] mx-auto mt-5">
        <form onSubmit={handleSubmit}>
          <Input
            value={input}
            onChange={handleInputChange}
            type="text"
            role="input"
            className="w-full h-10 pl-3 pr-8 text-base placeholder-gray-600 rounded-2xl  bg-gray-800"
            placeholder="Regular input"
            label="Title"
          />
          <Button
            type="submit"
            className="ml-2"
            text="Add"
            disabled={input.trim() === ""}
          />
        </form>
      </div>
    </div>
  );
};

export default Form;
