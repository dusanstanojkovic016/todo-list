import React, { useState, useEffect } from "react";
import ButtonIcon from "../UICOMPONENTS/ButtonIcon";
import { TrashIcon, PencilIcon } from "@heroicons/react/24/outline";
import { useSnackbar } from "notistack";
import Form from "./Form";
import ApiService from "../../axios";

const TodoSingle = ({
  text,
  createdAt,
  onDelete,
  _id,
  onClick,
  onEdit,
  fetchData,
  completed,
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [isEditing, setIsEditing] = useState(false);
  const [completed1, setCompleted1] = useState(completed);
  const [pickedColor, setPickedColor] = useState("");

  const shadowColors = [
    "shadow-violet-500",
    "shadow-blue-500",
    "shadow-indigo-500",
    "shadow-orange-700",
    "shadow-pink-500",
    "shadow-purple-500",
  ];

  useEffect(() => {
    const randomShadowColor =
      shadowColors[Math.floor(Math.random() * shadowColors.length)];
    setPickedColor(randomShadowColor);
  }, []);

  const handleDeleteClick = () => {
    onDelete(_id);
    console.log(text);
    enqueueSnackbar("Item has been deleted.", {
      variant: "error",
      autoHideDuration: 2000,
    });
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
  };

  const handleTodoClick = async () => {
    console.log(_id);
    try {
      const response = await ApiService().put(`/todos/${_id}`, {
        completed: !completed1,
      });
      console.log(response);
      setCompleted1(!completed);
    } catch (error) {
      console.error(error);
    }
  };

  // const handleTodoClick = async () => {
  //   try {
  //     const response = await ApiService().put(`/todos/${_id}`, {
  //       completed: !completed,
  //     });
  //     const updatedTodo = response.data;
  //     setCompleted(updatedTodo.completed);
  //     setTodos((prevTodos) =>
  //       prevTodos.map((todo) =>
  //         todo._id === updatedTodo._id ? updatedTodo : todo
  //       )
  //     );
  //   } catch (err) {
  //     console.log(err.message);
  //   }
  // };

  const handleFormSubmit = async (input) => {
    console.log("Form submitted with input:", input);
    try {
      await onEdit(_id, input);
      setIsEditing(false);
    } catch (err) {
      console.log(err.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, [completed]);

  return (
    <div
      className={`p-4 ${pickedColor} shadow-md bg-gray-950 opacity-100 rounded-lg flex justify-between items-center`}
      onClick={handleTodoClick}
    >
      {isEditing ? (
        <Form
          onFormSubmit={handleFormSubmit}
          initialText={text}
          onCancel={handleCancelEdit}
        />
      ) : (
        <>
          <div className="flex flex-col">
            <h3
              className={`text-violet-600 ${
                completed1 ? "line-through decoration-slate-50" : ""
              }`}
            >
              {text}{" "}
            </h3>
            <h4 className="text-gray-400">{createdAt}</h4>
          </div>
          <div className="flex items-center">
            <ButtonIcon onClick={handleEditClick}>
              <PencilIcon className="w-5 h-5 text-white static mr-3" />
            </ButtonIcon>
            <ButtonIcon onClick={handleDeleteClick}>
              <TrashIcon className="w-5 h-5 text-white static ml-14" />
            </ButtonIcon>
          </div>
        </>
      )}
    </div>
  );
};

export default TodoSingle;
