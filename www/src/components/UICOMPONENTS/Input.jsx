import React from "react";

const Input = ({
  onChange,
  className,
  type,
  placeholder,
  role,
  label,
  ...rest
}) => {
  return (
    <div>
      <input
        autoFocus
        onChange={onChange}
        className={className}
        placeholder={placeholder}
        type={type}
        role={role}
        label={label}
        {...rest} // ovde bi trebalo da je value
      />
    </div>
  );
};

export default Input;
