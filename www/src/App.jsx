import React from "react";
import Todo from "./components/Todo/Todo";
import "./App.css";
import Form from "./components/Todo/Form";
import Navbar from "./components/UICOMPONENTS/Navbar";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className="bg-black h-[100vh] p-0 m-0 relative">
      {/* <h1 className="text-white font-barlow text-3xl">HELLO NEW WORLD!</h1> */}

      <Navbar />
      <Routes>
        <Route path="/add" element={<Todo />} />
      </Routes>
    </div>
  );
}

export default App;
